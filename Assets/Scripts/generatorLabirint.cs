using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generatorLabirint : MonoBehaviour
{
    public GameObject block; //Наш блок земли
    public GameObject grass; //Наша трава
    private GameObject hero; //Наш герой
    private List<int> razmer = new List<int>(){20,10}; //Наша размерность с Х и У
    private GameObject floor; //Группа объектов block

    void Awake(){
        floor = GameObject.Find("floor"); //Присваевает игровой объект floor переменной
    }

    void Start(){
        for (int x = 0; x < razmer[0]; x++){
            for (int y = 0; y < razmer[1]; y++){
                if (x == 0 || y == 0 || x == razmer[0]-1 || y == razmer[1]-1){
                    GameObject copyBlock = Instantiate(block);
                    copyBlock.transform.SetParent(floor.transform.GetChild(0),true);
                    copyBlock.transform.localScale = block.transform.localScale;
                    copyBlock.transform.position = new Vector3(0 + 1f*x,0+1f*y,block.transform.position.z);
                    copyBlock.transform.name = "stone";
                }
                else{
                    GameObject copyGrass = Instantiate(grass);
                    copyGrass.transform.SetParent(floor.transform.GetChild(1),true);
                    copyGrass.transform.localScale = copyGrass.transform.localScale;
                    copyGrass.transform.position = new Vector3(0 + 1f*x,0+1f*y,block.transform.position.z);
                    copyGrass.transform.name = "grass";
                }
            }
        }
    }
}
